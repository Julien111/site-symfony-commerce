<?php

namespace App\Controller;

use App\Service\SendMailService;
use App\Form\ResetPasswordFormType;
use App\Repository\UsersRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Form\ResetPasswordRequestFormType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

class SecurityController extends AbstractController
{
    #[Route(path: '/connexion', name: 'app_login')]
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    #[Route(path: '/logout', name: 'app_logout')]
    public function logout(): void
    {
        throw new \LogicException('Cette méthode peut être vide, elle sera interceptée par la clé de déconnexion de votre pare-feu.');
    }

    #[Route('/oubli-mot-passe', name:'forgotten_password')]
    public function forgottenPassword(Request $request, UsersRepository $usersRepository, TokenGeneratorInterface $generator, EntityManagerInterface $manager, SendMailService $mail): Response
    {
        $form = $this->createForm(ResetPasswordRequestFormType::class);
        $form->handleRequest($request);
                
       
        if ($form->isSubmitted() && $form->isValid()) {
            //chercher l'utilisateur par son email
            $user = $usersRepository->findOneByEmail($form->get('email')->getData());
            
            if($user){
                //si on a un utilisateur on doit générer un token de réinitialisation
                //on crée le token
                $token = $generator->generateToken();
                $user->setResetToken($token);

                $manager->persist($user);
                $manager->flush();

                //on génére un lien pour changer le mot de passe et on crée un mail
                $url = $this->generateUrl('reset_pass', ['token' => $token], UrlGeneratorInterface::ABSOLUTE_URL);
                //contexte 

                $context = compact('url', 'user');
                
                //envoie de mail
                $mail->send(
                    "no-reply@e-commerce.net",
                    $user->getEmail(),
                    'Réinitialiser votre mot de passe',
                    'password_reset',
                    $context
                );
                $this->addFlash('success', 'Vous allez recevoir un mail pour réinitialiser votre mot de passe.');
                return $this->redirectToRoute('app_login');
            }
            //$user est null
            $this->addFlash('danger', 'Un problème est survenu');
            return $this->redirectToRoute('app_login');
            
        }

        return $this->render('security/reset_password_request.html.twig', [
            'requestPassForm' => $form->createView(),
        ]);
    }

    #[Route(path: '/oubli-mot-passe/{token}', name: 'reset_pass')]
    public function resetPass(string $token, Request $request, UsersRepository $usersRepository, EntityManagerInterface $entityManager, UserPasswordHasherInterface $userPasswordHasher): Response
    {
        //on vérifie le token avec la méthode symfony
        $user = $usersRepository->findOneByResetToken($token);

        if($user){
            $form = $this->createForm(ResetPasswordFormType::class);
            $form->handleRequest($request);
                
        //si le form est validé respecter ordre isSubmitted et isValid
        if ($form->isSubmitted() && $form->isValid()) {
            //on efface le token 
            $user->setResetToken('');
            //on hash le password
            $user->setPassword(
            $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );
            //on valide
            $entityManager->persist($user);
            $entityManager->flush();
            $this->addFlash('success', 'Votre mot de passe a été changé avec succès.');
            return $this->redirectToRoute('app_login');
        }

        return $this->render('security/reset_password.html.twig', [
            'passForm' => $form->createView(),
        ]);
        
        }
        //$user est null
        $this->addFlash('danger', 'Le lien est invalide');
        return $this->redirectToRoute('app_login');
        
    }
}