<?php

namespace App\Controller;

use App\Entity\Users;
use App\Service\JWTService;
use App\Service\SendMailService;
use App\Form\RegistrationFormType;
use App\Repository\UsersRepository;
use App\Security\UsersAuthenticator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;

class RegistrationController extends AbstractController
{
    #[Route('/inscription', name: 'app_register')]
    public function register(Request $request, UserPasswordHasherInterface $userPasswordHasher, UserAuthenticatorInterface $userAuthenticator, UsersAuthenticator $authenticator, EntityManagerInterface $entityManager, SendMailService $mail, JWTService $jwtService): Response
    {
        $user = new Users();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
            $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $entityManager->persist($user);
            $entityManager->flush();
            // do anything else you need here, like send an email

            // envoie d'un token JWT 

            // header 
            $header = [
                'typ' => 'JWT',
                'alg' => 'HS256',
            ];
            //on crée le paylad 

            $payload = [                
                "user_id" => $user->getId(),                
            ];
            //on génère le token 
            $token = $jwtService->generate($header, $payload, $this->getParameter('app.jwtsecret'));
            
            //envoie de mail
            $mail->send(
                "no-reply@e-commerce.net",
                $user->getEmail(),
                'Activation de votre compte sur le site e-commerce',
                'register',
                compact('user', 'token')
            );

            return $userAuthenticator->authenticateUser(
                $user,
                $authenticator,
                $request
            );
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

    #[Route('/verif/{token}', name: 'verify_user')]
    public function verifyUser($token, JWTService $jwt, UsersRepository $usersRepository, EntityManagerInterface $manager): Response
    {
        //on doit vérifier que le token n'a pas été modifié, expiration et s'il est valide
        if($jwt->isValid($token) && !$jwt->isExpired($token) && $jwt->checkToken($token, $this->getParameter('app.jwtsecret'))){
            //on récupère le payload pour avoir l'id du user
            $payload = $jwt->getPayload($token);
            $user = $usersRepository->find($payload['user_id']);

            //on vérifie que l'utilisateur existe et que le compte n'a pas été vérifié
            if($user && $user->getIsVerified() === false) {
                $user->setIsVerified(true);
                $manager->flush($user);
                $this->addFlash('success', 'Votre compte est validé.');
                return $this->redirectToRoute('profile_index');
            }
        }
        // sinon problème

        $this->addFlash('danger', 'Le token est invalide ou à expirer !');
        return $this->redirectToRoute('app_login');
    }

    #[Route('/renvoieToken', name: 'resend_token')]
    public function resendToken(JWTService $jwtService, SendMailService $mail, UsersRepository $userRepository): Response
    {
        $user = $this->getUser();

        if(!$user){
            $this->addFlash('danger', 'Vous devez être connecté pour arriver à cette page !');
            return $this->redirectToRoute('app_login');
        }

        if($user->getIsVerified()){
            $this->addFlash('warning', 'Votre compte est déjà activé.');
            return $this->redirectToRoute('profile_index');
        }

        // envoie d'un token JWT 

        // header 
        $header = [
            'typ' => 'JWT',
            'alg' => 'HS256',
        ];
        //on crée le paylad 
        $payload = [                
            "user_id" => $user->getId(),                
        ];
        //on génère le token 
        $token = $jwtService->generate($header, $payload, $this->getParameter('app.jwtsecret'));
        
        //envoie de mail
        $mail->send(
            "no-reply@e-commerce.net",
            $user->getEmail(),
            'Activation de votre compte sur le site e-commerce',
            'register',
            compact('user', 'token')
        );

        $this->addFlash('success', 'Email de vérification envoyé.');
        return $this->redirectToRoute('profile_index');
    }
}