<?php

namespace App\Controller;

use App\Entity\Products;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/produits', name: 'products_')]
class ProductsController extends AbstractController
{
    #[Route('/', name: 'index')]
    public function index(): Response
    {
        return $this->render('products/index.html.twig', [
            'controller_name' => 'Products',
        ]);
    }

    #[Route('/details', name: 'details')]
    public function details(): Response
    {
        return $this->render('products/details.html.twig', [
            'controller_name' => 'Détail',
        ]);
    }
}