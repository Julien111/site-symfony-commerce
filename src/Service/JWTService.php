<?php

namespace App\Service;

class JWTService 
{
    /**
     * function generate jwt web token
     *
     * @param array $header
     * @param array $payload
     * @param string $secret
     * @param integer $validity
     * @return string
     */
    public function generate(array $header, array $payload, string $secret, int $validity = 10800): string 
    {
        // la validité ne peut pas être < 0
        if($validity > 0){
            // stock date 
            $now = new \DateTimeImmutable();
            //on prend la date avec getTimestamp() et on ajoute la validité et on a la date d'expiration
            $exp = $now->getTimestamp() + $validity;
            
            $payload['iat']= $now->getTimestamp();
            $payload['exp']= $exp;
        }

        //on encode en base 64
        $base64Header = base64_encode(json_encode($header));
        $base64Payload = base64_encode(json_encode($payload));

        //on nettoie les valeurs encodées (retrait des +, / et = ) mais pour le token JWT marche pas
        $base64Header = str_replace(['+', '/', '='], ['-', '_', ''], $base64Header);
        $base64Payload = str_replace(['+', '/', '='], ['-', '_', ''], $base64Payload);        

        //on doit générer la signature
        $secret = base64_encode($secret);

        $signature = hash_hmac('sha256', $base64Header . '.' . $base64Payload, $secret, true);
        //on encode la signature en base 64 
        $base64Signature = base64_encode($signature);
        $base64Signature = str_replace(['+', '/', '='], ['-', '_', ''], $base64Signature);

        // On crée le token
        $jwt = $base64Header . '.' . $base64Payload . '.' . $base64Signature;

        return $jwt;
    }

    //on vérifie le token 
    /**
     * function isValid() 
     * She verify if the token is valid
     * 
     * @param string $token
     * @return boolean
     */
    public function isValid(string $token): bool 
    {
        return preg_match(
            '/^[a-zA-Z0-9\-\=\_]+\.[a-zA-Z0-9\-\=\_]+\.[a-zA-Z0-9\-\=\_]+$/', $token) === 1;
    }

    //on récupère le payload 
    /**
     * function getPayload() 
     * This function get the payload and decode
     *
     * @param string $token
     * @return array
     */
    public function getPayload(string $token): array
    {
        //on démonte le token à chaque '.'point

        $tokenArray = explode('.', $token);           

        //on décode le payload 

        $payload = json_decode(base64_decode($tokenArray[1]), true);

        return $payload;
    }

     //on récupère le header
    /**
     * function getHeader() 
     * This function get the header and decode
     *
     * @param string $token
     * @return array
     */
    public function getHeader(string $token): array
    {
        //on démonte le token à chaque '.'point

        $tokenArray = explode('.', $token);

        //on décode le header

        $header = json_decode(base64_decode($tokenArray[0]), true);

        return $header;
    }

    //on vérifie la date exp
    /**
     * function
     * Verify the date of expiration
     *
     * @param string $token
     * @return boolean
     */
    public function isExpired(string $token){

        $payload = $this->getPayload($token);

        $now = new \DateTimeImmutable();

        return $payload['exp'] < $now->getTimestamp();
    }

    //on vérifie la signature du token
    /**
     * function check the token
     *
     * @param string $token
     * @param string $secret
     * @return boolean
     */
    public function checkToken(string $token, string $secret): bool
    {
        //on récupère le header et payload
        $header = $this->getHeader($token);
        $payload = $this->getPayload($token);

        //on récupère le token 
        $verifToken = $this->generate($header, $payload, $secret, 0);
        
        return $token === $verifToken;
    }
    
}