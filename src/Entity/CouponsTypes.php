<?php

namespace App\Entity;

use App\Repository\CouponsTypesRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CouponsTypesRepository::class)]
class CouponsTypes
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 100)]
    private $name;

    #[ORM\OneToMany(mappedBy: 'couponsTypes', targetEntity: CouponsElements::class, orphanRemoval: true)]
    private $couponsElements;

    public function __construct()
    {
        $this->couponsElements = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, CouponsElements>
     */
    public function getCouponsElements(): Collection
    {
        return $this->couponsElements;
    }

    public function addCouponsElement(CouponsElements $couponsElement): self
    {
        if (!$this->couponsElements->contains($couponsElement)) {
            $this->couponsElements[] = $couponsElement;
            $couponsElement->setCouponsTypes($this);
        }

        return $this;
    }

    public function removeCouponsElement(CouponsElements $couponsElement): self
    {
        if ($this->couponsElements->removeElement($couponsElement)) {
            // set the owning side to null (unless already changed)
            if ($couponsElement->getCouponsTypes() === $this) {
                $couponsElement->setCouponsTypes(null);
            }
        }

        return $this;
    }
}
