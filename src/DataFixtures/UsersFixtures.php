<?php

namespace App\DataFixtures;

use App\Entity\Users;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Faker;

class UsersFixtures extends Fixture
{

    public function __construct(private UserPasswordHasherInterface $passwordEncoder, private SluggerInterface $slugger)
    {

    }
    
    public function load(ObjectManager $manager): void
    {
        $admin = new Users();
        $admin->setLastname('Matew');
        $admin->setFirstname('Laurent');
        $admin->setEmail('Laurent@test.com');
        $admin->setAdress("12 rue du port");
        $admin->setZipcode("89000");
        $admin->setCity("Auxerre");
        $admin->setPassword($this->passwordEncoder->hashPassword($admin, 'admin1234'));
        $admin->setRoles(['ROLE_ADMIN']);
        $admin->setIsVerified(true);
        $manager->persist($admin);

        $faker = Faker\Factory::create('fr_FR');

        for($i = 1; $i <= 5; $i++){
            $user = new Users();
            $user->setLastname($faker->lastname);
            $user->setFirstname($faker->firstname);
            $user->setEmail($faker->email);
            $user->setAdress($faker->streetAddress);
            $user->setZipcode(str_replace(' ', '', $faker->postcode));
            $user->setCity($faker->city);
            $user->setPassword($this->passwordEncoder->hashPassword($admin, 'secretuser'.$i));
            $user->setIsVerified(true);
            $manager->persist($user);
        }

        $manager->flush();
    }
}