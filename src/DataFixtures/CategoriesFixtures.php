<?php

namespace App\DataFixtures;

use App\Entity\Categories;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\String\Slugger\SluggerInterface;

class CategoriesFixtures extends Fixture
{

    private $counter = 1;
    
    public function __construct(private SluggerInterface $slugger)
    {
        
    }

    public function load(ObjectManager $manager): void
    {
        $parent = $this->createCategory("Informatique", null, $manager);
        
        $this->createCategory('Ordinateur portable', $parent, $manager);
        $this->createCategory('Ordinateur de bureau', $parent, $manager);
        $this->createCategory('Ecran', $parent, $manager);        
        
        $parent2 = $this->createCategory("Jeux vidéo", null, $manager);

        $this->createCategory('Consoles', $parent2, $manager);
        $this->createCategory('Les jeux PS4', $parent2, $manager);
        $this->createCategory('Les jeux PS5', $parent2, $manager);        

        $manager->flush();
        
    }

    //méthode pour éviter la répétition 

    public function createCategory(string $name, Categories $parent = null, ObjectManager $manager)
    {
        $category = new Categories();
        $category->setName($name);
        $category->setSlug($this->slugger->slug($category->getName())->lower());
        $category->setParent($parent);
        $manager->persist($category);

        $this->addReference('cat-'.$this->counter, $category);
        $this->counter++;

        return $category;
    }
}