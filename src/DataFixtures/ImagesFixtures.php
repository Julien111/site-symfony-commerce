<?php

namespace App\DataFixtures;

use Faker;
use App\Entity\Images;
use App\DataFixtures\ProductsFixtures;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class ImagesFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $faker= Faker\Factory::create('fr_FR');

        for($img = 1; $img <= 30; $img++){

            $image = new Images();
            $product = $this->getReference('prod-'.rand(1, 10));
            $image->setProducts($product);
            $image->setName("uploads/images/img.png");
            $manager->persist($image);           
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            ProductsFixtures::class
        ];  
    }
}